class TodosController < ApplicationController
  before_action :authenticate!

  def index
    @todos = Todo.where(email: @current_email)
  end

  def new
    @todo = Todo.new
  end

  def create
    @todo = Todo.new(todo_params)
    @todo.email = @current_email
    @todo.save
    redirect_to root_path and return
  end

  private
    def todo_params
      params.require(:todo).permit(:title, :email)
    end
end
